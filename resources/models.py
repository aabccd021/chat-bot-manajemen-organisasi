# coding=utf-8
from collections import namedtuple
from datetime import datetime

Group = namedtuple("Group",
                   ["id", "name", "descriptions", "schedules", "announcements"])

# Schedule = namedtuple("Schedule",
#                       ["name", "location", "date", "descriptions"])

class Schedule():
    def __init__(self, id, name, location, date, descriptions):
        self.id = id
        self.name = name
        self.location = location
        self.date = date
        self.descriptions = descriptions

    def __lt__(self, other):
        self_datetime = datetime.strptime(self.date, '%Y-%m-%dT%H:%M')
        other_datetime = datetime.strptime(other.date, '%Y-%m-%dT%H:%M')
        return self_datetime < other_datetime


Announcement = namedtuple("Announcement",
                          ["id", "title", "text"])

Workplan = namedtuple("Workplan",
                      ["id", "name", "person_in_charge", "descriptions", "is_done"])

Document = namedtuple("Document",
                      ["name", "exp", "descriptions", "data"])

User = namedtuple("User",
                  ["name", "bod", "skill", "id"])

File = namedtuple("File",
                  ["name", "url"])

Session = namedtuple("Session",
                     ["user_id", "organization_id", "session_type"])
