class Error(Exception):
    """Base class for exception in this module"""
    pass


class UserHasNoProfileError(Error):
    """Exception raised when user does not have profile yet"""
    pass
