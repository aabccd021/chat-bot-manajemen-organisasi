from __future__ import unicode_literals

import os

from flask import Flask, request, abort
from linebot import (LineBotApi, WebhookHandler, WebhookParser)
from linebot.exceptions import (InvalidSignatureError)


channel_secret = os.getenv('LINE_CHANNEL_SECRET')
channel_access_token = os.getenv('LINE_CHANNEL_ACCESS_TOKEN')
database_url = os.getenv('DATABASE_URL')

app = Flask(__name__)
line_bot_api = LineBotApi(channel_access_token)
handler = WebhookHandler(channel_secret)
parser = WebhookParser(channel_secret)


@app.route("/callback", methods=['POST'])
def callback():

    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    events = parser.parse(body, signature)
    # app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError as error:
        app.logger.info(error)
        abort(400)

    return 'OK'
