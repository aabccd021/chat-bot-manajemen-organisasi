import errno
import tempfile

import linebot
import sys
import os.path

from linebot.models import SourceGroup, SourceRoom, TextSendMessage

from resources.handler.basic_handler import basic_check
from resources.setup import line_bot_api

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")

from resources.help import ERROR_ORGANIZATION_NOT_REGISTERED, ERROR_NO_ACCESS, ERROR_NON_GROUP, ERROR_NOT_JOINED_ANY_ORG
from resources.database.user_db import user_already_in_organization, get_user_organizations
from resources.database.files_db import add_org_file,get_org_files
from resources.database.organization_db import exist_organization, get_organization_details
from resources.global_var import get_current_event
from resources.modules.user_interface import memilih_org, lihat_dokumen_organisasi

from resources.modules.fitur_olahdata import dbx,upload_dbx

from resources.models import File

UPLOAD_NEW_FILE_FORMAT = '''Silakan kirimkan dokumen melalui chat, Orgo akan \
memberikan link unduh dari file tersebut kepada para anggota grup, sehingga bisa \
diunduh kembali saat diperlukan'''

def reply_txt(event,msg):
    line_bot_api.reply_message(event.reply_token, TextSendMessage(text=msg))

def start_upload(event,document):
    print("Pengecekan berhasil, melanjutkan memproses file")
    tmp_path = os.path.join(os.path.dirname(__file__), event.source.group_id)

    try:
        print("Mencoba membuat directory sementara")
        os.makedirs(tmp_path)
        print("Berhasil membuat directory sementara, melanjutkan proses")
    except OSError as exc:
        print("Directory gagal dibuat")
        if exc.errno == errno.EEXIST and os.path.isdir(tmp_path):
            print('Directory sudah ada, melanjutkan proses')

    finally:
        print("Menulis file ke folder sementara")
        with tempfile.NamedTemporaryFile(dir=tmp_path, delete=False) as file:
            for chunk in document.iter_content():
                file.write(chunk)
            file_path = file.name

        print("Me-rename nama file")
        nama_list = event.message.file_name.split('_')
        del nama_list[0]
        nama_baru = '_'.join(nama_list)

        print("Me-rename directory sementara")
        dist_path = file_path + '-' + event.message.file_name
        path_split = dist_path.split('/')
        del path_split[-1]
        dist_path_baru = ('/'.join(path_split)) + '/' + nama_baru
        os.rename(file_path, dist_path_baru)

        print("Mencoba mengupload ke dropbox")
        path_part = dist_path_baru[4:]
        prosesUpload = upload_dbx(dbx, dist_path_baru, path_part)
        print("Upload berhasil, Memeriksa apakah file baru atau revisi")

        if isinstance(prosesUpload, list):
            print("File adalah baru, mulai menambahkan ke db...")
            file_link = prosesUpload[-1]
            respon = prosesUpload[0]

            print("LINK: " + file_link)
            print("RESPON: " + respon)
            file_db = File(name=nama_baru,url=file_link)
            prosesTambah = add_org_file(file_db,event.source.group_id)

            if prosesTambah == "OK":
                line_bot_api.reply_message(event.reply_token, TextSendMessage(text=respon))
            else:
                line_bot_api.reply_message(event.reply_token, TextSendMessage(text=prosesTambah))
        else:
            print("File adalah revisi atau ada masalah")
            line_bot_api.reply_message(event.reply_token, TextSendMessage(text=prosesUpload))

def upload_document(event,document):
    user_id = event.source.user_id

    if isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
        if exist_organization(group_id) and user_already_in_organization(user_id, group_id):
            start_upload(event,document)
        elif not exist_organization(event.source.group_id):
            reply_txt(event,ERROR_ORGANIZATION_NOT_REGISTERED)
        else:
            reply_txt(event,ERROR_NO_ACCESS)
    else:
        reply_txt(event,ERROR_NON_GROUP)

@basic_check
def get_document(group_id=None):
    event = get_current_event()

    if group_id is not None:
        files = get_org_files(group_id)
        organization_name = get_organization_details(group_id).name
        lihat_dokumen_organisasi(event, files, organization_name)

    elif isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
        files = get_org_files(group_id)
        organization_name = get_organization_details(group_id).name
        lihat_dokumen_organisasi(event, files, organization_name)

    else:
        try:
            user_id = event.source.user_id
            organizations = get_user_organizations(user_id)
            if len(organizations) == 0:
                reply_txt(event,ERROR_NOT_JOINED_ANY_ORG)
            elif len(organizations) > 20:
                reply_txt(event,"Mohon maaf, jumlah organisasi Anda melebihi batas (20)")
            else:
                ui = memilih_org(event,"dokumen",organizations)
                line_bot_api.reply_message(event.reply_token, ui)
                if isinstance(event.source, SourceRoom):
                    reply_txt(event,"Silakan cek personal chat Anda")
        except linebot.exceptions.LineBotApiError:
            reply_txt(event,"Mohon tambahkan Orgo sebagai teman dulu ya :) Nanti Orgo tidak dapat membalas pesan")
