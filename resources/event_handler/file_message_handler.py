from resources.event_handler.file_handler import upload_document
from resources.setup import line_bot_api


def handle_file_message(event):
    """
    Ini handler jika bot menerima pesan berupa FILE (message=FileMessage)

    :param event:
    :return:
    """

    print("Menerima file dari pengguna")
    file_terkirim = line_bot_api.get_message_content(event.message.id)
    print("Memeriksa prasyarat upload")
    upload_document(event, file_terkirim)
