# coding=utf-8
"""
Interface untuk mengakses database jadwal
"""
from datetime import datetime, date, timedelta

import psycopg2
from pytz import timezone

from resources.database.utilities import normalize_sql_varchar
from resources.global_var import get_database_conn
from resources.models import Schedule


def get_org_schedules(organization_id: str):
    """
    Mendapatkan semua jadwal pada suatu organisasai
    :param organization_id: id organisasi
    :return: list dari named-tuple jadwal
    """

    schedules = []

    conn = get_database_conn()

    cur = conn.cursor()

    cur.execute(
        """
        SELECT schedule_title,
                schedule_location,
                schedule_date,
                schedule_descriptions,
                schedule_id
        FROM organization_schedules
        WHERE organization_id = %s
        """, (organization_id,))

    rows = cur.fetchall()

    for row in rows:
        schedule_title = row[0]
        schedule_location = row[1]
        schedule_date = row[2]
        schedule_descriptions = row[3]
        schedule_id = row[4]
        schedule = Schedule(name=schedule_title,
                            location=schedule_location,
                            date=schedule_date,
                            descriptions=schedule_descriptions,
                            id=schedule_id)
        schedules.append(schedule)

    cur.close()

    return schedules


def add_org_schedule(schedule_tuple: Schedule, organization_id: str):
    """
    Menambahkan jadwal baru ke database.
    :param schedule_tuple: named tuple jadwal yang ingin ditambahkan
    :param organization_id: id organisasi yang dimasukkan jadwal
    :return: berhasil tidaknya memasukkan jadwal ke database
    """

    conn = get_database_conn()
    try:
        # conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()

        # print("inserting jadwal data to organisasi_jadwal table ...")
        cur.execute(
            """
            INSERT INTO organization_schedules (organization_id,
                                                schedule_title,
                                                schedule_date,
                                                schedule_location,
                                                schedule_descriptions)
            VALUES (%s,%s,%s,%s,%s)
            """, (organization_id,
                  schedule_tuple.name,
                  schedule_tuple.date,
                  schedule_tuple.location,
                  schedule_tuple.descriptions))

        # print("committing the changes to the database...")
        conn.commit()

        # print("closing communication with the database...")
        cur.close()

    except psycopg2.DatabaseError as error:
        # print("Found error")
        # print(error)
        return "Terjadi kesalahan saat Orgo hendak menambahkan jadwal, silakan coba kembali " \
               "beberapa saat lagi "

    # finally:
    #     if conn is not None:
            # print("Closing connection...")
            # conn.close()
    # print("End inserting...")

    return "Terima kasih, " + schedule_tuple.name + " berhasil ditambahkan sebagai jadwal"


def show_all_schedule_in_string(schedules):
    schedules_string = "Jangan lupa ya besok ada :\n"
    for schedule in schedules:
        schedule_time = schedule.date.split("T")[1]
        schedule_title = schedule.name
        schedules_string += ("\n\n"
                             + schedule_title + "\n"
                             + schedule_time
                             )
    return schedules_string


def filter_tomorrow(schedules):
    filtered_schedules = []
    for schedule in schedules:
        date = schedule.date.split("T")[0]
        tomorrow = get_tomorrow()
        if date == tomorrow:
            filtered_schedules.append(schedule)
    return filtered_schedules


def filter_future(schedules):
    filtered_schedules = []
    for schedule in schedules:
        # print(schedule.date)
        schedule_year = int(schedule.date.split("T")[0].split("-")[0])
        schedule_month = int(schedule.date.split("T")[0].split("-")[1])
        schedule_day = int(schedule.date.split("T")[0].split("-")[2])
        schedule_hour = int(schedule.date.split("T")[1].split(":")[0])
        schedule_minute = int(schedule.date.split("T")[1].split(":")[1])
        now = datetime.now(timezone("Asia/Jakarta")).replace(tzinfo=None)
        schedule_datetime = datetime(year=schedule_year,
                                     month=schedule_month,
                                     day=schedule_day,
                                     hour=schedule_hour,
                                     minute=schedule_minute)
        # print(schedule_datetime)
        # print(now)
        if schedule_datetime > now:
            filtered_schedules.append(schedule)
    return filtered_schedules


def get_tomorrow():
    tomorrow = datetime.now(timezone("Asia/Jakarta")) + timedelta(days=1)
    return (str(tomorrow).split(" ")[0])


def get_user_schedules(user_id):
    from resources.database.organization_db import get_all_user_organizations_id
    user_organizations_id = get_all_user_organizations_id(user_id)

    user_schedules = []

    for user_organization_id in user_organizations_id:
        user_schedule = get_org_schedules(user_organization_id)
        user_schedules.extend(user_schedule)
    # print(user_schedules)
    return user_schedules


def get_db_schedule(schedule_id):
    conn = get_database_conn()
    cur = conn.cursor()

    cur.execute(
        """
        SELECT schedule_title,
                schedule_date,
                schedule_location,
                schedule_descriptions
        FROM organization_schedules
        WHERE schedule_id = %s
        """, (schedule_id,))
    schedule = cur.fetchone()
    schedule = Schedule(name=schedule[0],
                        date=schedule[1],
                        location=schedule[2],
                        descriptions=schedule[3],
                        id=schedule_id
                        )
    cur.close()
    return schedule

def update_schedule_db(attribute: str, value: str, id: str):
    attributes_list = ["title", "location", "date", "descriptions"]

    if attribute not in attributes_list:
        # print("Wrong Attribute Error")
        return

    conn = get_database_conn()
    cur = conn.cursor()

    sql = ("UPDATE organization_schedules "
           + "SET schedule_" + attribute + " = '" + value + "'"
           + "WHERE schedule_id = " + id)

    cur.execute(sql)
    conn.commit()
    cur.close()


def delete_schedule_db(id: str):
    conn = get_database_conn()
    cur = conn.cursor()

    cur.execute(
        """
        DELETE FROM organization_schedules
        WHERE schedule_id = %s
        """,(id,))
    conn.commit()
    cur.close()


if __name__ == '__main__':
    update_schedule_db("title", "Tutup Bersama", "12")
