# coding=utf-8
"""
Interface untuk mengakses database proker
"""
import psycopg2

from resources.database.utilities import normalize_sql_varchar, normalize_sql_tuple
from resources.global_var import get_database_conn, set_database_conn
from resources.models import Workplan


def get_org_workplans(organization_id):
    """
    Mendapatkan semua proker organisasi
    :param organization_id: id organisasi
    :return: list dari named-tuple proker
    """

    workplans = []

    conn = get_database_conn()
    cur = conn.cursor()

    cur.execute(
        """
        SELECT workplan_name, workplan_pic, workplan_descriptions, workplan_is_done, workplan_id
        FROM organization_workplans
        WHERE organization_id = %s
        """, (organization_id,))

    rows = cur.fetchall()
    # print(rows)
    for row in rows:
        # print(row)
        workplan_name = row[0]
        workplan_person_in_charge = row[1]
        workplan_descriptions = row[2]
        workplan_is_done = row[3]
        workplan_id = row[4]
        workplan = Workplan(name=workplan_name,
                            person_in_charge=workplan_person_in_charge,
                            descriptions=workplan_descriptions,
                            is_done=workplan_is_done,
                            id=workplan_id)
        workplans.append(workplan)

    cur.close()

    return workplans


def add_org_workplan(proker: Workplan, organization_id: str):
    """
    Menambahkan proker baru ke sebuah organisasi
    :param proker: named-tuple proker
    :param organization_id: id organisasi
    :return: berhasil tidaknya penambahan proker
    """

    conn = get_database_conn()

    cur = conn.cursor()

    cur.execute(
        """
    INSERT INTO organization_workplans (organization_id,
    workplan_name,
    workplan_pic,
    workplan_descriptions,
    workplan_is_done)
    VALUES (%s,%s,%s,%s,%s);
    """, (organization_id,
          proker.name,
          proker.person_in_charge,
          proker.descriptions,
          "false"))

    conn.commit()
    cur.close()

    return "Terima kasih, proker tersebut berhasil didaftarkan"


def workplan_bool_to_string(bool: str):
    if bool == "true":
        return "Sudah tuntas"
    elif bool == "false":
        return "Belum tuntas"


def set_done_workplan(workplan_id: str, is_done: str):
    conn = get_database_conn()
    cur = conn.cursor()
    cur.execute(
        """
        UPDATE organization_workplans
          SET workplan_is_done = %s
          WHERE workplan_id = %s
        """, (is_done, workplan_id))
    cur.close()
    conn.commit()


def delete_workplan(workplan_id: str):
    if isinstance(workplan_id, int):
        workplan_id = int(workplan_id)
    conn = get_database_conn()
    cur = conn.cursor()
    cur.execute(
        """
        DELETE FROM organization_workplans
          WHERE workplan_id = %s
        """, (workplan_id,))
    cur.close()
    conn.commit()


def get_workplan_name(workplan_id: str):
    conn = get_database_conn()
    cur = conn.cursor()
    cur.execute(
        """
        SELECT workplan_name
        FROM organization_workplans
        WHERE workplan_id = %s
        """, (workplan_id,))
    workplan_name = cur.fetchone()[0]
    cur.close()
    conn.commit()
    return workplan_name


def get_db_workplan(workplan_id: str):
    conn = get_database_conn()
    cur = conn.cursor()

    # print("c")
    cur.execute(
        """
        SELECT workplan_name,workplan_pic,workplan_descriptions,workplan_is_done
        FROM organization_workplans
        WHERE workplan_id = %s
        """, (workplan_id,))
    workplan = cur.fetchone()
    # print("d")
    # print(workplan)
    workplan = Workplan(name=workplan[0],
                        person_in_charge=workplan[1],
                        descriptions=workplan[2],
                        is_done=workplan[3],
                        id=workplan_id
                        )
    cur.close()
    return workplan


def get_workplans_progress(workplans):
    # workplans = get_org_workplans(organization_id)
    total = len(workplans)
    done_total = 0
    for workplan in workplans:
        if workplan.is_done == "true":
            done_total += 1

    if done_total == 0:
        return str(0)
    result = int(100*done_total / total)
    return str(result)


def update_workplan_db(attribute: str, value: str, id: str):
    attributes_list = ["name", "pic", "descriptions"]

    if attribute not in attributes_list:
        print("Wrong Attribute Error")
        return

    conn = get_database_conn()
    cur = conn.cursor()

    sql = ("UPDATE organization_workplans "
           + "SET workplan_" + attribute + " = '" + value + "'"
           + "WHERE workplan_id = " + id)

    cur.execute(sql)
    conn.commit()
    cur.close()


